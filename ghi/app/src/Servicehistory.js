import React, { useEffect, useState } from 'react';

export default function Servicehistory() {
    const [appointments, setAppointments] = useState([]);

    const [filterVIN, setFilterVIN] = useState('');

    const fetchData = async () => {

        const url = "http://localhost:8080/api/appointments/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const formattedAppointments = data.Appointments.map(appointment => {
                const dateTime = new Date(appointment.date_time);
                const date = dateTime.toDateString();
                const time = dateTime.toLocaleTimeString();
                return {
                    ...appointment,
                    date,
                    time,
                };
            });
            setAppointments(formattedAppointments);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const filteredAppointments = appointments.filter(appointment =>
        appointment.vin.includes(filterVIN)
    );

    return (
        <div>
            <div className="row">
                <h1>Service History</h1>
            </div>
            <input
                type="text"
                placeholder="Enter VIN to filter"
                value={filterVIN}
                onChange={(e) => setFilterVIN(e.target.value)}
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>VIP?</th>
                        <th>customer</th>
                        <th>Data</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.VIP ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}
