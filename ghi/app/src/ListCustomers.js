import React, { useEffect, useState } from 'react';

export default function ListCustomers() {
    const [customers, setCustomer] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setCustomer(data.customers)
        }else{
          console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchData();
       }, []);

       return (
        <>
        <div className="row">
        <h1>Customers</h1>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer Name</th>
            <th>Address</th>
            <th>Phone Number</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(Customer => {
            return (
              <tr key={ Customer.id }>
                <td>{ Customer.first_name } { Customer.last_name }</td>
                <td>{ Customer.address }</td>
                <td>{ Customer.phone_number }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
}
