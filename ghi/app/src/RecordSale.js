import { useState, useEffect } from "react"

export default function RecordSale() {
  const [price, setPrice] = useState("")
  const [vin, setVin] = useState("")
  const [autos, setVins] = useState([])
  const [salesperson1, setSalesperson] = useState("")
  const [salesperson, setSalespeople] = useState([])
  const [customer, setCustomer] = useState("")
  const [customers, setCustomers] = useState([])

  const handleSubmit = async (event) => {
      event.preventDefault()
      const data = {}
      data.price = price
      data.automobile = vin
      data.salesperson = salesperson1
      data.customer = customer
      const url = "http://localhost:8090/api/sales/"
      const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
              "Content-Type": "application/json",
          }
      }
      const response = await fetch(url, fetchConfig)

      if(response.ok) {
          setPrice("");
          setVin("");
          setVins([]);
          setSalesperson("");
          setSalespeople([]);
          setCustomer("");
          setCustomers([])
      }
      else {
          console.error("Sale was not created")
      }
  }

  const fetchCustomerData = async () => {
      const url = "http://localhost:8090/api/customers/"
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          setCustomers(data.customers)
      }
  }

  useEffect(() => {
      fetchCustomerData();
    }, []);

  const fetchSalesPeopleData = async () => {
      const url = "http://localhost:8090/api/salespeople/"
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          setSalespeople(data.salesperson)
      }
  }

  useEffect(() => {
      fetchSalesPeopleData();
    }, []);

  const fetchVinData = async () => {
      const url = "http://localhost:8100/api/automobiles/"
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          setVins(data.autos)
      }
  }

  useEffect(() => {
      fetchVinData();
    }, []);


  const handleCustomerChange = (event) => {
      const value = event.target.value
      setCustomer(value)
  }


  const handleSalespersonChange = (event) => {
      const value = event.target.value
      setSalesperson(value)
  }

  const handleVinChange = (event) => {
      const value = event.target.value
      setVin(value)
  }

  const handlePriceChange = (event) => {
      const value = event.target.value
      setPrice(value)
  }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Submit a new Sale!</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
                <select onChange={handleVinChange} value={vin} required id="vin" name="vin" className="form-select">
                  <option value="">Choose a Vehicle Vin Number</option>
                  {autos.map(auto => {
                    return (
                        <option key ={auto.id} value={auto.vin}>
                            {auto.vin}
                        </option>
                    )
                  })}
                </select>
                <select onChange={handleSalespersonChange} value={salesperson1} required id="salesperson" name="salesperson" className="form-select">
                  <option value="">Choose a Salesperson</option>
                  {salesperson.map(salesperson => {
                    return (
                        <option key ={salesperson.id} value={salesperson.employee_id}>
                            {salesperson.employee_id}
                        </option>
                    )
                  })}
                </select>
                <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                  <option value="">Choose a Customer</option>
                  {customers.map(customer => {
                    return (
                        <option key ={customer.id} value={customer.first_name}>
                            {customer.first_name}
                        </option>
                    )
                  })}
                </select>
                <div>
              <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
        </>
    )
}
