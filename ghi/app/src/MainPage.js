import './mainpage.css'
import car from '/app/src/images/carcar.jpg'
function MainPage() {


  return (
    <div id="main">
      <img id="image" src={car} alt=''></img>
      <h1 id="header">CarCar!</h1>
      <div id="paracon">
        <p>
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
