import { useEffect, useState } from "react";

export default function ListAutomobiles() {
    const [autos, setAutomobiles] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url)

        if(response.ok){
            const data = await response.json()
            setAutomobiles(data.autos)
        }else{
            console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchData();
       }, []);

       return (
        <>
        <div className="row">
        <h1>Automobile Inventory</h1>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {autos.map(auto => {
            return (
              <tr key={ auto.id }>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.model.manufacturer.name }</td>
                <td>{ String(auto.sold) }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
}
