import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import SalespersonDetail from "./SalespersonDetail"


export default function SalespersonHistory() {
    const [salesperson, setSalespeople] = useState([])
    const [Salesperson, setSalesperson] = useState("")
    const navigate = useNavigate()

    const handleSalesperson = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salesperson)
        }
    }

    useEffect(() => {
        fetchData();
       }, []);

       async function redirectToDetail(Salesperson) {
        navigate(`/salespersonhistory/${Salesperson}`)
        window.location.reload(false)
       }

       return (
        <>
        <h1>Choose a salesperson</h1>
        <select onChange={handleSalesperson} value={Salesperson} required id="salesperson" name="salesperson" className="form-select">
        <option value="">Salespeople</option>
        {salesperson.map(salesperson => {
            return (
                <option key ={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name + " " + salesperson.last_name}
                </option>
            )
        })}
        </select>
        <button onClick={() => redirectToDetail(Salesperson)}>Submit</button>
        <SalespersonDetail/>
        </>
       )
}
