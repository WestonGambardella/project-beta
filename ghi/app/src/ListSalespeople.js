import React, { useEffect, useState } from 'react';

export default function ListSalespeople() {
  const [salesperson, setSalespeople] = useState([])

  const fetchData = async () => {
      const url = "http://localhost:8090/api/salespeople/"
      const response = await fetch(url)

      if (response.ok) {
          const data = await response.json()
          setSalespeople(data.salesperson)
      }else{
        console.error("Bad Request")
      }
  }

  useEffect(() => {
      fetchData();
      }, []);

  return (
      <>
      <div className="row">
      <h1>Sales Force</h1>
      </div >
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {salesperson?.map(Salespeople => {
          return (
            <tr key={ Salespeople.id }>
              <td>{ Salespeople.employee_id }</td>
              <td>{ Salespeople.first_name } { Salespeople.last_name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
  }
