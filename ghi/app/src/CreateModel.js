import { useEffect, useState } from "react";

export default function CreateModel() {
    const [name, setName] = useState("")
    const [picture_url, setPicture_url] = useState("")
    const [manufacturer, setManufacturer] = useState("")
    const [manufacturers, setManufacturers] = useState([])

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handlePicture_UrlChange = (event) => {
        const value = event.target.value
        setPicture_url(value)
    }

    const handleManufacturer = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const fetchManufacturersData = async () => {
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url)

        if(response.ok){
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }else{
            console.error("Bad request")
        }
    }

    useEffect(() => {
        fetchManufacturersData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer_id = manufacturer
        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(url, fetchConfig)

        if(response.ok){
            setName("")
            setPicture_url("")
            setManufacturer("")
        }else{
            console.error("Bad Request")
        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Submit a new Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture_UrlChange} value={picture_url} placeholder="URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleManufacturer} value={manufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                  <option value="">Choose a Manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                        <option key ={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Submit</button>
              </form>
          </div>
        </div>
      </div>
        </>
    )
}
