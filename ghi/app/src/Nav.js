import { NavLink } from 'react-router-dom';

function Nav() {
  return (
  <div>
    <nav className="navbar navbar-expand-xl navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <NavLink className="navbar-brand" to="/salespeople">Salespeople</NavLink>
        <NavLink className="navbar-brand" to="/unsoldinventory">Unsold Cars</NavLink>
        <NavLink className="navbar-brand" to="/listmanufacturers">List Manufacturers</NavLink>
        <NavLink className="navbar-brand" to="/listmodels">List Models</NavLink>
        <NavLink className="navbar-brand" to="/createmodel">Create Model</NavLink>
        <NavLink className="navbar-brand" to="/listautomobiles">List Automobiles</NavLink>
        <NavLink className="navbar-brand" to="/createmanufacturer">Create Manufacturer</NavLink>
        <NavLink className="navbar-brand" to="/Createappointment">Create Appointment</NavLink>
        <NavLink className="navbar-brand" to="/Addtechnician">Create technician</NavLink>
        <NavLink className="navbar-brand" to="/Servicehistory">Service History</NavLink>
      </div>
      </nav>
      <nav className="navbar navbar-expand-xl navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/createsalesperson">Create Salesperson</NavLink>
        <NavLink className="navbar-brand" to="/createautomobile">Create Automobile</NavLink>
        <NavLink className="navbar-brand" to="/customers">List of Customers</NavLink>
        <NavLink className="navbar-brand" to="/createcustomer">Create New Customer</NavLink>
        <NavLink className="navbar-brand" to="/createsale">Create New Sale</NavLink>
        <NavLink className="navbar-brand" to="/listsales">List of Sales</NavLink>
        <NavLink className="navbar-brand" to="/salespersonhistory">Salespeople History</NavLink>
        <NavLink className="navbar-brand" to="/ListAppointment">Appointments</NavLink>
        <NavLink className="navbar-brand" to="/listtechnician">Technicians</NavLink>
      </div>
    </nav>
  </div>
  )
}

export default Nav;
