<h1>About the project</h1>
    <p>This application's purpose is to digitize the data of a car dealership. From organizing inventory to scheduling appointments for maintenance, this application handles all functionality regarding a car dealership.</p>


<h1>Built With</h1>
    <ul>Postgres</ul>
    <ul>ReactJs</ul>
    <ul>Docker</ul>
    <ul>Django</ul>
    <ul>Bootstrap</ul>

<h1>Getting Started</h1>
<p>Just a few things before you dive into the project!</p>
    <h2>Prerequisites</h2>
    <ul>Docker Desktop</ul>
    <h2>Installation</h2>
    <ul>Clone the repository into a directory of your choosing</ul>
    <ul>Use "docker volume build beta-data" command to build the volume</ul>
    <ul>Use the "docker-compose build" command to build the necessary images</ul>
    <ul>Then use the "docker-compose up" command to create the containers</ul>
    <ul>Head over to http://localhost:3000/ to interact with the application!</ul>
    <h2>Usage</h2>
    <p>There are three main parts to this application; sales, service, and the inventory. To start, lets take a look at inventory.</p>
    <h3>Inventory</h3>
    <p>For us to record a sale or schedule an appointment for diagnostic testing, we need to have an inventory to interact with! To start, we need to: </p>
    <ul>1. Create a manufacturer</ul>
    <ul>2. Create a model using the manufacturer just created</ul>
    <ul>3. Create an automobile with the information that was just submitted</ul>
    <ul>4. Repeat steps 1-3 twice more so our inventory has at least three automobiles in it</ul>
    <ul>Now off to sales!</ul>
    <h3>Sales</h3>
    <ul>1. Create a salesperson that we will use to attribute the sale to.</ul>
    <ul>2. Navigate to the list of unsold cars and copy one of the vin numbers.</ul>
    <ul>3. Create a new sale and attribute the sale to the new salesperson you created.</ul>
    <ul>4. Now, check the unsold cars menu and you should not see the vin number of the autombile that was just sold. Also, check the salesperson history by checking the name of the salesperson in the menu. You should see the price, customer name, and vin number of the automobile sold.</ul>
    <ul>Next, services!</ul>
    <h3>Services</h3>
    <ul>1. Create a technician that will handle the service.</ul>
    <ul>2. Create an appointment using the technician just created</ul>
    <ul>3. Navigate to service history and type the vin number in the search bar. You should see the appointment history of the vehicle, along with manipulating the status of the appointment.</ul>
