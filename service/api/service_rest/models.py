from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=200, unique=True)
    customer = models.CharField(max_length=200)
    reason = models.CharField(max_length=500, null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="Appointments",
        on_delete=models.CASCADE
    )
