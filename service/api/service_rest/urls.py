from django.urls import path
from service_rest.views import (
    TechnicianPollView,
    TechnicianPolldelete,
    AppointmentPollView,
    AppointmentsPolldelete,
    AppointmentsPollfinished,
    AppointmentsPollcanceled,
)


urlpatterns = [
    path("technicians/", TechnicianPollView, name="technicianPollView"),
    path("technicians/<int:id>/", TechnicianPolldelete, name="technicianPolldelete"),
    path("appointments/", AppointmentPollView, name="AppointmentPollView"),
    path("appointments/<int:id>/", AppointmentsPolldelete, name="AppointmentPollView"),
    path("appointments/<int:id>/finish/", AppointmentsPollfinished, name="AppointmentsPollfinished"),
    path("appointments/<int:id>/cancel/", AppointmentsPollcanceled, name="AppointmentsPollcanceled"),
]
