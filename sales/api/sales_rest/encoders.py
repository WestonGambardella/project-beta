from common.json import ModelEncoder

from.models import SalespersonModel, CustomerModel, AutomobileVO, SaleModel


class SalespersonEncoder(ModelEncoder):
    model = SalespersonModel
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class CustomerModelEncoder(ModelEncoder):
    model = CustomerModel
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]


class SaleModelEncoder(ModelEncoder):
    model = SaleModel
    properties = [
        "price",
        "id",
        "automobile",
        "salesperson",
        "customer"
    ]
    encoders = {
        "automobile":AutomobileVOEncoder(),
        "salesperson":SalespersonEncoder(),
        "customer":CustomerModelEncoder()
    }
